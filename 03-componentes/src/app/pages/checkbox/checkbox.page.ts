import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.page.html',
  styleUrls: ['./checkbox.page.scss'],
})
export class CheckboxPage implements OnInit {
  data = [
    { valor: 'Pepperoni', selected: true },
    { valor: 'Sausage', selected: false },
    { valor: 'Mushroom', selected: false }
  ];

  constructor() { }

  ngOnInit() {
  }

  onClick( item: any ){
    console.log(item);
  }

  verData(){
    console.log(this.data);
  }
}
