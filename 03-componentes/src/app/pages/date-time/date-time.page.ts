import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-date-time',
  templateUrl: './date-time.page.html',
  styleUrls: ['./date-time.page.scss'],
})
export class DateTimePage implements OnInit {

  fechaNac: Date = new Date(); //Igual a la fecha actual
  customYearValues = [2020, 2016, 2008, 2004, 2000, 1996];
  customDayShortNames = ['s\u00f8n', 'man', 'tir', 'ons', 'tor', 'fre', 'l\u00f8r'];

  customPickerOptions={
    buttons: [{
      text: 'Save',
      handler: (event) => {
        console.log("Guardado");
        console.log(event);
      }
    }, {
      text: 'Cancel',
      handler: () => {
        console.log('Cancelado');
      }
    }]
  }

  constructor() { }

  ngOnInit() {
  }

  cambio(event){
    console.log(event);
    console.log( new Date( event.detail.value ) );
  }

}
