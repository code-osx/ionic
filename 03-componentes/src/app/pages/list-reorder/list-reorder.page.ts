import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-reorder',
  templateUrl: './list-reorder.page.html',
  styleUrls: ['./list-reorder.page.scss'],
})
export class ListReorderPage implements OnInit {

  abc: string[] = ['A','E','I','O','U'];
  constructor() { }
  state: boolean = true;

  ngOnInit() {
  }

  doReorder( event: any ){
    console.log(event);

    console.log("Antes: ", this.abc);
    const itemMover = this.abc.splice( event.detail.from, 1 )[0];
    this.abc.splice( event.detail.to, 0, itemMover );
    console.log("Ahora: ", this.abc);

    event.detail.complete();
  }

  changeState(){
    this.state = !this.state;
  }
}
