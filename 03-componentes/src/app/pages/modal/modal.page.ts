import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalInfoPage } from '../modal-info/modal-info.page';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

  constructor( private modalController: ModalController) { }

  ngOnInit() {
  }

  async presentModal(){
    const modal = await this.modalController.create({
      component: ModalInfoPage,
      componentProps: {
        nombre: 'Oscar',
        pais: 'Mexico'
      },
      cssClass: 'my-custom-class'
    });
    await modal.present();

    const resp = await modal.onDidDismiss(); //recibe los datos despues de la animacion del cierre del modal
    //const resp = await modal.onDidDismiss(); recibe los datos antes de la animacion del cierre del modal
    
    console.log(JSON.stringify(resp)); //JSON.stringify(), funciona para poder ver los logs en android studio
  }

}
