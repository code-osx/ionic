import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-infinite',
  templateUrl: './infinite.page.html',
  styleUrls: ['./infinite.page.scss'],
})
export class InfinitePage implements OnInit {

  data: any[] =  Array(20);
  //@ViewChild( IonInfiniteScroll ) infiniteScroll: IonInfiniteScroll;

  constructor() { }

  ngOnInit() {
  }

  loadData( event ){
    console.log(event);
    
    setTimeout(() => {
      const nuevoArr = Array(20);
      this.data.push( ...nuevoArr );

      event.target.complete();
      // this.infiniteScroll.complete() elimina el event y puedes hacer referencia al event con el viewChild

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.data.length == 100) {
        event.target.disabled = true;
        //this.infiniteScroll.disabled = true;
      }
    }, 1000);

  }
}
