import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-refresher',
  templateUrl: './refresher.page.html',
  styleUrls: ['./refresher.page.scss'],
})
export class RefresherPage implements OnInit {

  items: any[] = [];
  constructor() { }

  ngOnInit() {
  }

  doRefresh( event ){
    setTimeout(() => {
      this.items = Array(20);
      event.target.complete(); //tambien se puede inyectar el componente ion-refresh y evitar usar el event (mono en algunos complementos anteriores)
    }, 3000);
  }

}
event