import { Component, ViewChild } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { Article } from 'src/app/interfaces/interfaces';
import { NewsService} from '../../services/news.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  @ViewChild(IonSegment) segment: IonSegment;

  categories = ['business', 'entertainment', 'general', 'health', 'science', 'sports', 'technology'];
  news: Article[] = [];
  
  constructor( private newsService: NewsService ) {}

  ionViewDidEnter() {
    this.segment.value = this.categories[0];
    this.loadNews( this.segment.value );
  }

  categoryChange(event){
    this.news = [];
    
    this.loadNews( event.detail.value );
  }

  loadNews( category: string, event? ){
    this.newsService.getTopHeadLinesCategory( category ).subscribe( resp => {
      //console.log( resp );
      this.news.push( ...resp.articles );

      if( event ){
        event.target.complete();
      }
    });
  }

  loadMoreNews( event ){
    this.loadNews( this.segment.value , event);
  }

}
