import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Query } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RespuestaTopHeadlines } from '../interfaces/interfaces';

const apiKey = environment.apikey;
const apiUrl = environment.apiUrl;

const headers = new HttpHeaders({
  'X-Api-key': apiKey
});

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  headlinesPage = 0;

  currentCategory = '';
  categoryPage= 0;

  constructor( private http: HttpClient ) { }

  private consumirApi<T>( url: string){
    const fullUrl = apiUrl + url;
    return this.http.get<T>( fullUrl, {headers} );
  }

  getTopHeadLines(){
    this.headlinesPage++;
     return this.consumirApi<RespuestaTopHeadlines>('/top-headlines?country=us&page='+this.headlinesPage);
    //return this.http.get<RespuestaTopHeadlines>('https://newsapi.org/v2/top-headlines?country=us&apiKey=1a0f34595d6d461c8a318a192047cb14');
  }

  getTopHeadLinesCategory(category: string){
    if ( this.currentCategory == category ){
      this.categoryPage++;
    }
    else{
      this.categoryPage = 1;
      this.currentCategory = category;
    }
    return this.consumirApi<RespuestaTopHeadlines>('/top-headlines?country=us&category='+category+'&page='+this.categoryPage);
    //return this.http.get<RespuestaTopHeadlines>('https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=1a0f34595d6d461c8a318a192047cb14');
  }
}
