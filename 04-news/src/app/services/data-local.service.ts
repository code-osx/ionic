import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Article } from '../interfaces/interfaces';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  news: Article [] = [];

  constructor(private storage: Storage, private toastController: ToastController) {
    this.loadFavorites();
  }

  async presentToast(mess: string) {
    const toast = await this.toastController.create({
      message: mess,
      duration: 2000
    });
    toast.present();
  }

  saveNew(neww: Article){
    const exist = this.news.find(newAlm => newAlm.title == neww.title);

    if( !exist ){
      this.news.unshift(neww);
      this.storage.set('favorites', this.news);
    }
    
    this.presentToast('It was added to favorites');
  }

  async loadFavorites(){
    const favorites = await this.storage.get('favorites');

    if( favorites ){
      this.news = favorites;
    }

  }

  deleteNew(neww: Article){
    this.news = this.news.filter( newAlt => newAlt.title != neww.title );
    this.storage.set('favorites', this.news);

    this.presentToast('It was deleted from favorites');
  }
}