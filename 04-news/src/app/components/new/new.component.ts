import { Component, Input, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ActionSheetController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { DataLocalService } from 'src/app/services/data-local.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss'],
})
export class NewComponent implements OnInit {

  @Input() new: Article;
  @Input() indice: number;
  @Input() favoriteTab;

  constructor( private iab: InAppBrowser, private actionSheetController: ActionSheetController, private socialSharing: SocialSharing, private dataLocalService: DataLocalService ) { }

  ngOnInit() {}

  openNew(){
    const browser = this.iab.create(this.new.url, '_system');
  }

  async showMenu(){
    let saveDelBtn;

    if(this.favoriteTab){
      saveDelBtn = {
                      text: 'Delete',
                      icon: 'trash',
                      cssClass: 'action-dark',
                      handler: () => {
                        this.dataLocalService.deleteNew(this.new);
                      }
                    };
    } else{
      saveDelBtn = {
                      text: 'Favorite',
                      icon: 'star',
                      cssClass: 'action-dark',
                      handler: () => {
                        this.dataLocalService.saveNew(this.new);
                      }
                    };
    }

    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Share',
        icon: 'share-social',
        cssClass: 'action-dark',
        handler: () => {
          this.socialSharing.share(
            this.new.title,
            this.new.source.name,
            '',
            this.new.url
          );
        }
      }, 
      saveDelBtn,
      {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        cssClass: 'action-dark',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}
