import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input() mensaje: any; //Recibir informacion de componente padre
  @Output() clickPost = new EventEmitter<number>(); //Enviar informacion a componente padre

  constructor() { }

  ngOnInit(): void {
  }

  onClick(){
    this.clickPost.emit( this.mensaje.id );
  }
}
