import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';



@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  constructor(private dataService: DataService) { }

  mensajes: any;

  ngOnInit(): void {
    this.mensajes = this.dataService.getPosts();

    /*
    Esta forma funciona de la misma manera que la de arriba, pero... aqui nos suscribimos al observable
    y podemos observar la estructura de la informacion que se recibe y guardamos esa infromacion en mensajes.
    En la primera solo igualamos la variable al observable que recibimos, pero no podemos ver su estructura, para 
    poder verla basta con quitar el comentario de data.service.ts
    this.dataService.getPosts().subscribe( (posts: any[]) => {
      console.log(posts)
      this.mensajes = posts;
    } ) */
  }

  escuhaClick( id: number ){
    console.log("Click en: ", id);
  }

}
